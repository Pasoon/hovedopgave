﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IsoNordAppen
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Minside : ContentPage
    {
        public Minside()
        {
            InitializeComponent();

        }
        private async void NavigationTilMainPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage { });
            //throw new NotImplementedException();
        }
        private async void NavigationTilMinSide(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Minside { });
            //throw new NotImplementedException();
        }
        private async void NavigationTilUgeRegistrering(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new UgeRegistrering { });
            //throw new NotImplementedException();
        }

    }
}