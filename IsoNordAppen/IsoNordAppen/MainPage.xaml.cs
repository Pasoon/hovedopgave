﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IsoNordAppen
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


        }
        private async void NavigationTilMainPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage { });
            //throw new NotImplementedException();
        }
        private async void NavigationTilMinSide(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Minside { });
            //throw new NotImplementedException();
        }
        private async void NavigationTilUgeRegistrering(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new UgeRegistrering { });
            //throw new NotImplementedException();
        }



    }
}
