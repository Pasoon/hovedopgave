﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IsoNordAppen
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent(); 
            
            //MainPage = new NavigationPage(new UgeRegistrering());
            //MainPage = new UgeRegistrering();
            MainPage = new NavigationPage(new MainPage());
            //MainPage = new Minside();
            //MainPage = new Login();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
